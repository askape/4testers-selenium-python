from selenium.webdriver.common.by import By


class ProjectPropertiesPage:

    def __init__(self, browser):
        self.browser = browser

    def click_projects(self):
        self.browser.find_element(By.CSS_SELECTOR, "a[href='http://demo.testarena.pl/administration/projects']").click()
