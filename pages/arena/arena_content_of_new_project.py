from selenium.webdriver.common.by import By
from utils.random_message import generate_random_text

class ContentOfNewProject:
    def __init__(self, browser):
        self.browser = browser

    def insert_data_to_the_project(self, random_title):
        self.browser.find_element(By.CSS_SELECTOR, '#name').send_keys(random_title)
        self.browser.find_element(By.CSS_SELECTOR, '#prefix').send_keys(generate_random_text(5))
        self.browser.find_element(By.CSS_SELECTOR, '#description').send_keys(generate_random_text(40))

    def save_project(self):
        self.browser.find_element(By.CSS_SELECTOR, 'input#save').click()
