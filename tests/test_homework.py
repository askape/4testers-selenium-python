import pytest
from selenium.webdriver import Chrome, Keys
from webdriver_manager.chrome import ChromeDriverManager
from pages.arena.arena_project_properties_page import ProjectPropertiesPage
from pages.arena.arena_home_page import ArenaHomePage
from pages.arena.arena_login import ArenaLoginPage
from pages.arena.arena_project_page import ArenaProjectPage
from utils.random_message import generate_random_text
from pages.arena.arena_content_of_new_project import ContentOfNewProject


@pytest.fixture
def browser():
    driver = Chrome(executable_path=ChromeDriverManager().install())
    driver.get('http://demo.testarena.pl/zaloguj')
    arena_login_page = ArenaLoginPage(driver)
    arena_login_page.login('administrator@testarena.pl', 'sumXQQ72$L')
    yield driver
    driver.quit()

def test_should_add_new_project_page(browser):
    arena_project_page = ArenaProjectPage(browser)
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.click_tools_icon()
    arena_project_page.add_project()

    arena_content_of_new_project = ContentOfNewProject(browser)
    random_title = generate_random_text(10)
    arena_content_of_new_project.insert_data_to_the_project(random_title)
    arena_content_of_new_project.save_project()

    arena_project_properties_page = ProjectPropertiesPage(browser)
    arena_project_properties_page.click_projects()
    arena_project_page.search_for_created_project(random_title)
